import { combineReducers } from 'redux';

import storeProduct from './storeProduct'
import cart from './cart'
import loading from './loading'
import currentPetshop from './currentPetshop'
import modal from './modal'
import loginModal from './loginModal'
import user from './user'
import products from './products'
import mobileDrawer from './mobileDrawer'
import mobileCartDrawer from './mobileCartDrawer'

export default combineReducers({
    storeProduct,
    cart,
    loading,
    currentPetshop,
    modal,
    loginModal,
    user,
    products,
    mobileDrawer,
    mobileCartDrawer
})