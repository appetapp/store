import INITIAL_STATE from '../initialState';

export default function user(state = INITIAL_STATE.user, action) {
    console.log("action", action)
    switch (action.type) {
        case 'DO_LOGIN':
            console.log("logando", action.user)
            state = action.user;
            state.isUserLogged = true;
            state.favoritePetId = "5ec189b45b7df331e28f04ff";
            return state;
        case 'DO_LOGOUT':
            return {
                name: null,
                role: null,
                isUserLogged: false
            }
        default:
            return state;
    }
}