import INITIAL_STATE from '../initialState';

export default function loginModal(state = INITIAL_STATE.loginModal, action) {
    switch (action.type) {
        case 'CHANGE_TO_EMAIL_OPTION':
            return {
                isEmail: true,
                isQRCode: false

            }
        case 'CHANGE_TO_QRCODE_OPTION':
            return {
                isEmail: false,
                isQRCode: true
            }
        case 'CHANGE_TO_DEFAULT':
            return {
                isEmail: false,
                isQRCode: false
            }
        default:
            return state;
    }
}