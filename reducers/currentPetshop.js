import INITIAL_STATE from '../initialState';
import { TableFooter } from '@material-ui/core';

export default function currentPetshop(state = INITIAL_STATE.currentPetshop, action) {
    console.log("== petShop redux", action)
    switch (action.type) {
        case 'FETCHED_PETSHOP_DATA_SUCCESS':
            state = action.payload;
            console.log("== state", state)
            return state;
        default:
            return state;
    }
}