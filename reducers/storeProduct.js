import INITIAL_STATE from '../initialState';

export default function storeProduct(state = INITIAL_STATE.petshopList, action) {
    switch (action.type) {
        case 'FETCHED_PETSHOP_LIST_SUCCESS':
            return state = action.payload;
        case 'FETCHING_PETSHOP_DATA_REQUEST':
            return state.filter(petshop => petshop.name == action.petshopId);
        default:
            return state;
    }
}