import INITIAL_STATE from '../initialState';

const addItemToCart = (state, item) => {
    if (state.items.length == 0) {
        console.log("brincando", state.items)
        item['qtd'] = 1;
        return item;
    }
    console.log("brincando state", state)
    return state;
}

export default function cart(state = INITIAL_STATE.cart, action) {
    switch (action.type) {
        case 'ADD_TO_CART':
            var isExisting = state.products.filter(product => product.id === action.productItem.id);

            if (isExisting.length > 0) {
                isExisting[0]['qtd']++;
                return {
                    ...state,
                    products: [...state.products],
                }

            } else {
                action.productItem['qtd'] = 1;

                return {
                    ...state,
                    products: [...state.products, action.productItem],
                }
            }

        case 'REDUCE_FROM_CART':
            var isExisting = state.products.filter(product => product.id === action.productItem.id);

            if (isExisting.length > 0) {
                if (isExisting[0]['qtd'] == 1) {
                    return {
                        ...state,
                        products: [...state.products.filter(x => x.id != action.productItem.id)],
                    }

                } else {
                    isExisting[0]['qtd']--;
                    return {
                        ...state,
                        products: [...state.products],
                    }
                }

            } else {
                action.productItem['qtd'] = 1;

                return {
                    ...state,
                    products: [...state.products, action.productItem],
                }
            }

        case 'REMOVE_FROM_CART':
            return {
                ...state,
                products: [...state.products.filter(x => x.id != action.productItem.id)],
            }
        default:
            return state;
    }
}