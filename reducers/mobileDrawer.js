import INITIAL_STATE from '../initialState';

export default function mobileDrawer(state = INITIAL_STATE.mobileDrawer, action) {
    switch (action.type) {
        case 'TOGGLE_DRAWER':
            return {
                isOpen: !state.isOpen
            };
        default:
            return state;
    }
}