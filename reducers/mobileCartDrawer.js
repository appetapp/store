import INITIAL_STATE from '../initialState';

export default function mobileCartDrawer(state = INITIAL_STATE.mobileCartDrawer, action) {
    switch (action.type) {
        case 'TOGGLE_CART_DRAWER':
            return {
                isOpen: !state.isOpen
            };
        default:
            return state;
    }
}