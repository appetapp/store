import INITIAL_STATE from '../initialState';

export default function modal(state = INITIAL_STATE.modal, action) {
    console.log("== modal state", state)
    switch (action.type) {
        case 'OPEN_MODAL':
            return{
                isOpen: true,
                children: action.children
            }
        case 'CLOSE_MODAL':
            return{
                isOpen: false,
                children: null
            }
        default:
            return state;
    }
}