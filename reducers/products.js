import INITIAL_STATE from '../initialState';

export default function products(state = INITIAL_STATE.productList, action) {
    switch (action.type) {
        case 'FETCHED_PRODUCTS_LIST_SUCCESS':
            state = action.payload
            return state
        default:
            return state;
    }
}