import axios from "axios";

// const baseURL = "http://localhost:8080";
const baseURL = "https://appertserver.herokuapp.com";

const api = axios.create({
    baseURL,
    timeout: 5000
});

export const apiGet = async (url) => {
    return await api.get(`/${url}`)
        .then(response => response.data)
        .catch(err => {
            console.log("== erro:", err)
            throw err;
        });
}

export const apiPost = async (url, payload) => {
    return await axios.post(`/${url}`, payload)
        .then(response => response.data)
        .catch(err => {
            console.log("== erro:", err)
            throw err;
        });
}