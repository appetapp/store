export function addToCart(productItem) {
    return {
        type: 'ADD_TO_CART',
        productItem
    }
}
export function reduceFromCart(productItem) {
    return {
        type: 'REDUCE_FROM_CART',
        productItem
    }
}

export function removeFromCart(productItem) {
    return {
        type: 'REMOVE_FROM_CART',
        productItem
    }
}