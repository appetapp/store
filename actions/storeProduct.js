export function addToCart(payload) {
    return {
        type: 'ADD_TO_CART',
        payload
    }
}

export function fetchingData() {
    return {
        type: 'FETCHING_PETSHOP_LIST_REQUEST'
    }
}
export function fetchingPetshopData(petshopId) {
    return {
        type: 'FETCHING_PETSHOP_DATA_REQUEST',
        petshopId
    }
}