export function doLogin(user) {
    return {
        type: 'DO_LOGIN',
        user
    }
}

export function doLogout() {
    return {
        type: 'DO_LOGOUT'
    }
}
