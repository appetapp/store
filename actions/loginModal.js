export function handleChangeToEmail() {
    return {
        type: 'CHANGE_TO_EMAIL_OPTION'
    }
}

export function handleChangeToQRCode() {
    return {
        type: 'CHANGE_TO_QRCODE_OPTION'
    }
}


export function handleChangeToDefault() {
    return {
        type: 'CHANGE_TO_DEFAULT'
    }
}

