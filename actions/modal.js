export function handleCloseModal() {
    return {
        type: 'CLOSE_MODAL'
    }
}

export function handleOpenModal(children) {
    return {
        type: 'OPEN_MODAL',
        children
    }
}