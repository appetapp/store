export default {
    loading: false,
    productList: [],
    cart: {
        products: [],
        totalValue: 0,
    },
    mobileDrawer: {
        isOpen: false
    },
    mobileCartDrawer: {
        isOpen: false
    },
    modal: {
        isOpen: false,
        children: null
    },
    loginModal: {
        isEmail: false,
        isQRCode: false
    },
    user: {
        name: null,
        role: null,
        isUserLogged: false,
        favoritePetId: null
    },
    productList: [],
    petshopList: [],
    currentPetshop: {
        id: null,
        name: null,
        productList: []
    }
}
