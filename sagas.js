import { all, fork } from 'redux-saga/effects';
import petshopSaga from './sagas/petshopSaga'
import productSaga from './sagas/productSaga'

export default function* root() {
    return yield all([
        fork(petshopSaga),
        fork(productSaga)
    ])
}