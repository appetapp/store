import { takeEvery, takeLatest, put, call, select, all, delay } from 'redux-saga/effects';
import { apiGet, apiPost } from "../api";

const BaseEntity = "products";

function* fetchProductsList() {
    console.log("== product saga")
    try {
        const payload = yield call(apiGet, BaseEntity);
        yield put({ type: 'FETCHED_PRODUCTS_LIST_SUCCESS', payload })
    } catch (error) {
        yield put({ type: 'FETCHED_PRODUCTS_LIST_FAILURE' }, error)
        console.log("== " + error);
    }

}


export default function* root() {
    return yield all([
        takeLatest('FETCHING_PRODUCTS_LIST_REQUEST', fetchProductsList),
    ])
}