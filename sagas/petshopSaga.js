import { takeEvery, takeLatest, put, call, select, all, delay } from 'redux-saga/effects';
import { apiGet, apiPost } from "../api";

const BaseEntity = "petshops";

function* fetchPetshopList() {
    try {
        const payload = yield call(apiGet, BaseEntity);
        yield put({ type: 'FETCHED_PETSHOP_LIST_SUCCESS', payload })
    } catch (error) {
        yield put({ type: 'FETCHED_PETSHOP_LIST_FAILURE' }, error)
        console.log("== " + error);
    }

}

function* fetchPetshopData(action) {
    try {
        const payload = yield call(apiGet, `${BaseEntity}/${action.petshopId}`);
        yield put({ type: 'FETCHED_PETSHOP_DATA_SUCCESS', payload })
    } catch (error) {
        yield put({ type: 'FETCHED_PETSHOP_DATA_FAILURE' }, error)
        console.log("== " + error);
    }

}

export default function* root() {
    return yield all([
        takeLatest('FETCHING_PETSHOP_LIST_REQUEST', fetchPetshopList),
        takeLatest('FETCHING_PETSHOP_DATA_REQUEST', fetchPetshopData)
    ])
}